import unittest

import gevent
import psycopg2

from psycogreen.gevent import ConnectionPool


class FakeConnection(object):

    rollback_called = False
    commit_called = False

    def rollback(self):
        self.rollback_called = True

    def commit(self):
        self.commit_called = True


class TestConnectionPool(unittest.TestCase):

    def setUp(self):
        super(TestConnectionPool, self).setUp()
        self._orig_connect = psycopg2.connect
        self.conn = FakeConnection()
        self.pool = ConnectionPool('bogus-dsn')
        psycopg2.connect = lambda **kwargs: self.conn
        self.addCleanup(self._restore_psycopg2_connect)

    def _restore_psycopg2_connect(self):
        psycopg2.connect = self._orig_connect

    def test_commit_is_called_on_success(self):
        with self.pool:
            self.assertTrue(isinstance(self.pool._local, gevent.local.local))
            self.assertEqual(self.conn, self.pool._local.con)
        self.assertTrue(self.conn.commit_called)

    def test_rollback_is_called_on_success(self):
        try:
            with self.pool:
                raise ValueError("anything")
        except ValueError:
            pass
        self.assertTrue(self.conn.rollback_called)

    def test_cannot_reenter_connection_pool(self):
        try:
            with self.pool:
                with self.pool:
                    pass
        except RuntimeError:
            pass
        else:
            self.fail("Should not be allowed to re-enter pool")

    def test_pool_locks_when_max_connections_reached(self):
        pool = ConnectionPool('bogus-dsn', max_con=1)
        def f():
            with pool:
                self.assertTrue(pool._sem.locked())

        gevent.joinall([gevent.spawn(f)])


if __name__ == "__main__":
    unittest.main()
